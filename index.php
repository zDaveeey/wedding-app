<?php
/**
 * Created by PhpStorm.
 * User: Daveeey
 * Date: 7/6/2016
 * Time: 9:22 AM
 */
include('inc/timetable.php');
include('inc/seatingplan.php');
?>
<html>
<head>
    <title>Wedding App</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div id="wrapper">

        <!-- Home -->
        <div class="page home-page" id="home">
            <div data-role="main">
                <p class="text-center"><img src="img/moderateres.png"/></p>

                <p class="text-center"><span class="btn btn-blue page-link" rel="story">Our Story</span></p>
                <p class="text-center"><span class="btn btn-blue page-link" rel="timetable">Timetable</span></p>
                <p class="text-center"><span class="btn btn-blue page-link" rel="seat-finder">Find My Seat</span></p>
                <p class="text-center"><span class="btn btn-blue page-link" rel="message">Send Message</span></p>
            </div>
        </div>
        <!-- / Home -->

        <!-- Our Story -->
        <div class="page" id="story">
            <div data-role="main">
                <p class="text-center"><img src="img/moderateres.png"/></p>
                <h3 class="text-center">Our Story</h3>
                <p>It was a warm, spring evening. Our eyes met across a crowded room, I reached out for Ellen's hand and we walked away together into our future...</p>
                <p>Too cliche? OK. Here are the missing details...</p>
                <p>It was Butlins, in Skegness, of all places. Ellen was on a friends Hen Do and I wasn't supposed to be there, I had been brought along to drive the works van back the same evening but was
                    convinced to stay over and go out with them that night.</p>
                <p>Turns out the Hen party and our event guys were up there together as they were all close friends! I first noticed Ellen whilst giving
                    credit to my boss for managing to get a dance with a very attractive young lass! Turns out that attractive young lass was Ellen and on further inspection she seemed less than impressed with the
                    current situation.</p>
                <P>Being the gentleman that I am, I took a moment (or two) to appreciate the awkwardness of the situation, before heading over to relieve the poor girl from the clutches of my boss.</p>
                <p>We had a little laugh and a chat, then went our separate ways.</p>
                <p>A few hours later we bumped into the large group again in another bar, immediately I decided that this was no longer a coincidence and that I was clearly meant to make something of this situation.</p>
                <p>So I did. </p>
                <p>I can't remember exactly how, but the night ended with me asking for her number! It didn't go well. Turns out Ellen was only prepared
                    to allow me to contact her outside of this crazy place if I returned the following night to find her!</p>
                <p>Challenge accepted.</p>
                <p>The following night I returned, but this time with reinforcements. After dinner, me and a few friends from the night before set off with one mission - find the soon to be Mrs Dave.</p>
                <p>We succeeded.</p>
                <p>For the next year we saw each other practically every day until we decided we would move in together.</p>
                <p>We rented our first house together with our good friend Stu (one of my reinforcements on that successful
                    night), where we stayed for 3 years.</p>
                <p>Stu moved out to save up for his own place, so we replaced him with a rabbit! A year living on our own together made us realise that we made an amazing couple
                    and we bought our first house together!</p>
                <p>2 Years on and here we are! 1 rabbit, 2 kitties and at OUR WEDDING!</p>

                <p class="text-center"><span class="btn btn-blue page-link return" rel="home">Return</span></p>
             </div>
        </div>
        <!-- / Our Story -->

        <!-- Timetable -->
        <div class="page" id="timetable">
            <div data-role="main">
                <p class="text-center"><img src="img/moderateres.png"/></p>
                <h3 class="text-center">Timetable</h3>

                <?php $timetable = new Timetable; ?>
                <?php echo $timetable->getTimetableFormatted(); ?>

                <p class="text-center"><span class="btn btn-blue page-link return" rel="home">Return</span></p>
            </div>
        </div>
        <!-- / Timetable -->


        <!-- Find My Seat -->
        <div class="page" id="seat-finder">
            <div data-role="main">
                <p class="text-center"><img src="img/moderateres.png"/></p>
                <h3 class="text-center">Find Your Seat</h3>

                <div id="find-seat">
                    <p class="title bg-grey text-center">Who do we have here?</p>
                    <p><input type="text" id="guest-name"/></p>
                    <p class="text-center"><span class="btn btn-green" id="find-seat-submit">Find My Table!</span></p>
                </div>

                <div id="seat-result" class="text-center">
                    <p class="text-center btn-green btn"></p>
                </div>

                <div id="map-seat-result">
                    <p>Map will go here</p>
                </div>

                <p class="text-center"><span class="btn btn-blue page-link return" rel="home">Return</span></p>
            </div>
        </div>
        <!-- / Our Story -->

        <!-- Message -->
        <div class="page" id="message">
            <div data-role="main">
                <p class="text-center"><img src="img/moderateres.png"/></p>
                <h3 class="text-center">Send us a Message</h3>

                <div id="msg-result"></div>
                <div id="msg-entry">
                    <p class="text-center">Type your name & message for the Bride & Groom below and click send!</p>
                    <p><input type="text" name="msgguestname" id="msgguestname" placeholder="Your Name"/></p>
                    <p><textarea name="msg" id="msg" placeholder="Your Message"></textarea></p>
                    <p class="text-center"><span class="btn btn-green btn-snd-msg">Send!</span></p>
                </div>
                <p class="text-center"><span class="btn btn-blue page-link return" rel="home">Return</span></p>
            </div>
        </div>
        <!-- / Message -->


    </div>

    <script type="text/javascript" src="js/jquery.3.1.0.min.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>