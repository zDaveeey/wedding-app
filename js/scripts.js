/**
 * Created by Daveeey on 7/5/2016.
 */
$(document).ready(function(){
    $('.page-link').click(function(){
        if($(this).attr('rel') == "home"){
            location.reload();
        } else {
            $('.page').each(function(){
                $(this).slideUp('600','swing');
            });
            $('#' + $(this).attr('rel')).slideDown('600','swing');
        }
    });

    $('#find-seat-submit').click(function(){
        var guestname = encodeURIComponent($('#guest-name').val());
        data = {
            name : guestname
        };
        $.ajax({
            url:'/wedding-app/getseatnumber.php',
            data: data,
            dataType: 'text',
            type: 'post',
            success:function(result){
                $('#seat-result').html(result).slideDown('slow','swing');
            }
        })

    });


    $('.btn-snd-msg').click(function(){
        var guestmsg = encodeURIComponent($('#msg').val());
        var guestname = encodeURIComponent($('#msgguestname').val());
        data = {
            msg : guestmsg,
            name : guestname
        };
        $.ajax({
            url:'/wedding-app/inc/sendmsg.php',
            data: data,
            dataType: 'text',
            type: 'post',
            success:function(result){
                var newHtml = "<p class='text-center'>Thankyou for your message! We will read them all as soon as we can!</p>";
                $('#msg-entry').hide();
                $('#msg-result').html(newHtml).slideDown('slow','swing');
            }
        });

    })
});
