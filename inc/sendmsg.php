<?php
/**
 * Created by PhpStorm.
 * User: Daveeey
 * Date: 9/30/2016
 * Time: 1:42 PM
 */
if(isset($_POST['msg']) && isset($_POST['name'])){
    $_POST['msg'] = urldecode($_POST['msg']);
    $_POST['name'] = urldecode($_POST['name']);

    $msgFrom = str_replace("%20", " ", strip_tags($_POST['name']));
    $msgFrom = str_replace("%26", " ", $msgFrom);

    $msgTitle = "message-" . $msgFrom . "-" . time();

    $file = fopen("../msg/" . $msgTitle . ".txt", "w") or die();
    $txt = str_replace("%20", " ",strip_tags($_POST['msg'])) . " From: " . $msgFrom;
    fwrite($file, $txt);
    fclose($file);
    return true;
} else {
    return false;
}

