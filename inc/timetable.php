<?php
/**
 * Created by PhpStorm.
 * User: Daveeey
 * Date: 7/22/2016
 * Time: 2:51 PM
 */
class Timetable {

    private $_timetable; /* Timetable Array */

    public function __construct(){

        $this->setTimetable('data/timetable.csv');
        return true;
    }

    public function setTimetable($_filename){
        $table = array();
        $file = fopen($_filename, 'r');
        while (($line = fgetcsv($file)) !== FALSE) {
                $table[] = $line;
        }
        fclose($file);

        $this->_timetable = $table;
        return true;
    }


    public function getTimetable(){
        return $this->_timetable;
    }

    public function getTimetableFormatted(){
        $table = $this->getTimetable();
        $strBuff = "";
        foreach ($table as $row){
            if($row[1] != ""){
                $strBuff .= "<p class='title bg-grey'>" . $row[0] . "</p><p>". nl2br($row[1]) ."</p>";
            }
        }

        return $strBuff;
    }


}