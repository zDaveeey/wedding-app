<?php
/**
 * Created by PhpStorm.
 * User: Daveeey
 * Date: 7/22/2016
 * Time: 2:51 PM
 */
class SeatingPlan {

    private $_seatingplan; /* Seating Plan Array */

    public function __construct(){

        $this->setSeatingPlan('data/seatingplan.csv');
        return true;
    }

    public function setSeatingPlan($_filename){
        $table = array();
        $file = fopen($_filename, 'r');
        while (($line = fgetcsv($file)) !== FALSE) {
            $table[] = $line;
        }
        fclose($file);

        $this->_seatingplan = $table;
        return true;
    }


    public function getSeatingPlan(){
        return $this->_seatingplan;
    }

    public function getSeatByName($name){
        $name = str_replace("%20", " ",$name);
        $seatNum = "<p>Ah, this is embarrassing.<br/>I cannot find you on the seating plan.</p><p>Try just your first name, or shout 'USHER... USHER!'.</p>";
        foreach($this->_seatingplan as $row){
            if($row['0'] == $name){
                $seatNum = "<p>Good Evening " . $name . ",<br/>We hope you are having a great time!</p><p>You are on table: <br/><strong>". $row[1]."</strong></p>";
            }
        }

        return $seatNum;

    }



}